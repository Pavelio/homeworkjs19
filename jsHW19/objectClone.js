function cloneDeepObject(obj) {
  let clone = {};
  for (let prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      if (typeof obj[prop] === "object") {
        clone[prop] = cloneDeepObject(obj[prop]);
      }
      else clone[prop] = obj[prop];
    }
  }
  return clone;
}
let myHonda = {
  color: "red",
  wheels: 4,
  engine: {
    cylinders: 4,
    size: 'hello world',
    ret: {
      cylinder: 'world',
      sizes: 6.8
    }
  }
};
console.log(cloneDeepObject(myHonda));